#!/bin/bash
#Created by Otaku_Dev
banner_created(){
printf "\n\e[1;31m

___     _       _  ____
\_ \ __/ \ |\  / \/  __\\
  \ \\/| | //  | ||  \/|
 _/ /__| \//   | ||  __/
/__/ \/\__/    \_/\_/

	\e[1;30mCreated By: \e[1;36mOtaku-dev\e[0m
\n
"
}

#Dependencies for the program
packages=('curl' 'html2text' 'pv')
#check installed packages
for packg in ${packages[@]};do
	#installed packages
	if [ -x $PREFIX/bin/$packg ];then
		clear
	#not installed packages
	else
		printf "\e[1;34m[\e[1;32m+\e[1;34m]\e[1;33mInstalling Packages \e[1;30m${packg}\e[1;33m...\e[0m\n"
		#install packages
		apt install $packg -y > /dev/null 2>&1
	fi
done


#check parameter
if [ ! -z $1 ];then
	#banner
	banner_created
	#variable
	url="https://www.ipaddress.my"


	#we request a web request and parse the interesting information
	printf "\e[1;33m\n";curl -s -d "ip=$1" $url|html2text|grep ':'|tr -d '[]'|grep -v "unknown"|pv -qL 100
	printf "\e[0m\n"
else
	clear
	nameProgram=$(basename $0)
	banner_created
	printf "\e[1;34m[\e[1;31m!\e[1;34m]\e[1;33mUse:\e[1;32m ${nameProgram} <ip-address>\e[0m\n"|pv -qL 30
	exit 0
fi
