**GetPublicSh**

- > Tool to obtain relevant information from a public IP


<details><summary>Installation and Use Termux</summary>


1. `git clone https://gitlab.com/Otaku-dev/getpublicsh.git`


2. `chmod +x getPublicsh.sh`


3. `bash getPublicsh.sh`

**Example Termux**

4. `bash getPublicsh.sh 8.8.8.8`
</details>

**Demonstration in Termux**

![demoTermux](https://gitlab.com/Otaku-dev/getpublicsh/-/raw/main/img_demo/DEMO.jpg)


<details><summary>Installation and Use Kali Linux</summary>


1. `git clone https://gitlab.com/Otaku-dev/getpublicsh.git`


2. `chmod +x getpublicsh.sh`


3. `sudo ./getpublicsh.sh`

**Example KaliLinux**

4. `sudo ./getpublicsh.sh 8.8.8.8`
</details>

**Demonstration in KaliLinux**

![demoKaliLinux](https://gitlab.com/Otaku-dev/getpublicsh/-/raw/main/img_demo/demoKALI.jpeg)

<details><summary>Creator</summary>

> Otaku-dev
</details>
